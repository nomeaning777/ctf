require 'socket'
require 'expect'
require 'highline'
require 'stringio'
require 'io/interactive'

module CTF
  module IOWithEcho
    attr_accessor :echo, :echo_input, :echo_output, :echo_color
    def echo_input
      @echo_input || STDOUT
    end

    def echo_output
      @echo_output || STDOUT
    end

    def echo_color
      @echo_color || true
    end

    def write(str)
      echo_output_print str
      super str
    end

    def read(length = nil, outbuf = nil)
      super(length, outbuf).tap {|result| echo_input_print(result || '') }
    end

    def interactive!(input = STDIN, output = STDOUT)
      @echo = false
      super input, output
    end

    def gets
      super.tap {|result| echo_input_print(result || '') }
    end

    def getc
      super.tap {|result| echo_input_print(result || '') }
    end

    # TODO: Don't ignore timeout
    def expect(pattern, timeout = 9999999)
      result = nil
      loop do
        if pattern.is_a?(Regexp)
          break if result && result.match(pattern)
        else
          break if result && result.end_with?(pattern)
        end
        data = getc
        break unless data
        if result
          result << data.chr
        else
          result = data.chr
        end
      end
      return result unless result
      if pattern.is_a?(Regexp)
        [result] + (result.match(pattern) || [nil]).to_a[1..-1]
      else
        [result]
      end
    end

    private
    def echo_output_print(str)
      if echo
        if echo_color
          echo_output.print HighLine.color(str, :yellow)
        else
          echo_output.print str
        end
        echo_output.flush
      end
    end

    def echo_input_print(str)
      if echo
        if echo_color
          echo_input.print HighLine.color(str, :green)
        else
          echo_input.print str
        end
        echo_input.flush
      end
    end
  end
end

class IO
  prepend CTF::IOWithEcho
end
