module CTF
  module Utils
    module StringWithXor
      def ^(s)
        if s.is_a?(String)
          chars.zip(s[0, length].chars).map{|a,b|
            b ? (a.ord ^ b.ord).chr : a
          }.join
        else
          s = s.to_i
          chars.map{|a|(a.ord^s).chr}.join
        end
      end
    end
  end
end

class String
  include CTF::Utils::StringWithXor
end
