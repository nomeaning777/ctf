require 'prime'
require 'pp'
module CTF
  module Math
    def sqrt?(n)
      sqrtint(n) ** 2 == n
    end

    def sqrtint(n)
      root(n, 2)
    end

    def root(n, k)
      low, high = 0, n + 1
      while low + 1 < high
        mid = (low + high) / 2
        if n < mid ** k
          high = mid
        else
          low = mid
        end
      end
      low
    end

    def mod_pow(a, n, mod)
      ret = 1
      while n > 0
        ret = (ret * a) % mod if n.odd?
        a = (a * a) % mod
        n >>= 1
      end
      ret
    end

    def extgcd(a,b)
      return [1,0] if b == 0
      y,x = extgcd(b, a % b)
      y -= (a/b)*x
      return [x,y]
    end

    def mod_inverse(a, mod)
      x,y = extgcd(a, mod)
      return x % mod
    end

    def chinese_remainder(m1,m2,a,b)
      return (m2 * a * mod_inverse(m2,m1) + m1 * b * mod_inverse(m1,m2)) % (m1 * m2)
    end

    # 離散対数 O(k ^ (1/2) * log(mod))
    def discrete_log(a, b, mod, k = nil)
      n = ::Math::sqrt(k || mod).ceil + 1
      p, q = 1, b
      inverse = mod_pow(mod_inverse(a, mod), n, mod)
      t = Hash.new
      n.times do |i|
        t[p] = i unless t.key?(q)
        p = p * a  % mod
      end
      n.times do |i|
        return i * n + t[q] if t.key?(q)
        q = (q * inverse) % mod
      end
      return nil # not found
    end

    # Pohlig-Hellman Algorithmによる離散対数
    # http://en.wikipedia.org/wiki/Pohlig%E2%80%93Hellman_algorithm
    # require mod is prime!
    def discrete_log2(g, e, mod)
      res = 0
      mod2 = 1
      prime_factorization2(mod - 1).each do |pi, ei|
        m = pi ** ei
        ng = mod_pow(g,(mod - 1) / m, mod)
        ne = mod_pow(e,(mod - 1) / m, mod)
        x = discrete_log(ng, ne, mod, m)
        res = chinese_remainder(mod2, m, res, x % m)
        mod2 *= m
      end
      return res
    end

    def check_prime(p, count = nil)
      return true if [2,3].include?(p)
      return false if p.even? || p < 2

      d, s = p - 1, 0
      d, s = d >> 1, s + 1 while d.even?

      count = [16, p.to_s(4).size].max unless count
      count.times do
        a = rand(2...(p - 1))
        return false if p.gcd(a) != 1
        if (x = mod_pow(a, d, p)) != 1
          return false unless (0...s).inject(false) do |res, r| 
            break true if(x == p - 1)
            x = x * x % p
            next false
          end
        end
      end
      return true
    end

    # 素因数分解(試し割り法)
    def prime_factorization(n)
      res = []
      Prime.each do |p|
        break if p * p > n
        cnt = 0
        cnt, n = cnt + 1, n / p while n % p == 0
        res << [p,cnt] if cnt > 0
      end
      res << [n, 1] if n > 1
      return res
    end

    # 素因数分解(Pollards-Rho)
    def prime_factorization2(n, max_sieve = 65536, rec = true)
      res = []
      Prime.each do |p|
        break if p > max_sieve
        while n % p == 0
          res << p
          n =  n / p
        end
      end
      if check_prime(n)
        res << n
      elsif n == 1
      else
        [1,51,73].each do |i|
          x,y,d = 2,2,1
          while d == 1
            x = (x * x + i) % n
            y = (((y * y + i) % n) ** 2 + i) % n
            d = n.gcd((x-y).abs)
          end
          next if d == n
          res += prime_factorization2(d, 0, false) + prime_factorization2(n / d, 0, false)
          break
        end
      end
      res = res.sort.uniq.map{|a| [a, res.count(a)]} if rec
      return res
    end

    def eulerphi(n, factorized = nil)
      factorized = prime_factorization2(n) unless factorized
      phi = n
      factorized.each do |p,k|
        phi = phi - phi / p
      end
      phi
    end

    # 元の位数の計算
    def order(g, mod)
      tmp = eulerphi(mod)
      ret = prime_factorization2(tmp)
      order = 1
      ret.each do |p, k|
        order *= p ** (k - (0..k).select{|i|
          mod_pow(g, tmp / p ** i, mod) == 1
        }.max)
      end
      order
    end

    module_function :sqrtint, :root, :sqrt?
    module_function :mod_pow, :extgcd, :mod_inverse, :chinese_remainder, :discrete_log, :check_prime
    module_function :discrete_log2
    module_function :prime_factorization
    module_function :prime_factorization2
  end
end
