require 'metasm'
module CTF
  module Rop
    class RelocatableELF
      attr :offset
      attr_reader :elf
      def initialize(filename, offset = 0)
        @offset = offset
        @elf = ::Metasm::ELF.decode_file(filename)
        @functions = {}
        @elf.symbols.find_all do |s|
          s.name and s.type == 'FUNC' && s.shndx != 'UNDEF' && s.bind == 'GLOBAL'
        end.each do |s|
          @functions[s.name] = s.value
        end
      end

      def function(name)
        if @functions.include? name.to_s
          @functions[name.to_s] + offset
        else
          raise RuntimeError.new("No such function #{name}")
        end
      end
    end

    class ELF < RelocatableELF
      def initialize(filename)
        super filename, 0
      end
    end
  end
end

