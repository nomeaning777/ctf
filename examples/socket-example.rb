require 'ctf'

TCPSocket.open(ARGV[0], ARGV[1]) do |s|
  s.echo = true # Enable debug output
  s.echo_color = true # Enable output with color(Default)

  s.expect(/hoge/) # Read until match /hoge/. See IO#expect
end
