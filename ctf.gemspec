# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ctf/version'

Gem::Specification.new do |spec|
  spec.name          = "nomeaning-ctf"
  spec.version       = CTF::VERSION
  spec.authors       = ["nomeaning"]
  spec.email         = ["nomeaning@mma.club.uec.ac.jp"]
  spec.summary       = %q{Utils for CTF}
  spec.description   = %q{Utils for CTF}
  spec.homepage      = "https://bitbucket.org/nomeaning777/ctf"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_dependency "io-interactive"
  spec.add_dependency "highline"
  spec.add_dependency "metasm", '~> 1.0.2'
end
