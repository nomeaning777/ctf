# coding: UTF-8
require 'spec_helper'
require 'ctf'
require 'tempfile'

describe CTF::IOWithEcho do
  describe '#read' do
    let(:utf8_io) do
      io = Tempfile.open
      io.set_encoding(Encoding::UTF_8, Encoding::UTF_8)
      io.puts 'ほげほげ'
      io.seek 0
      io
    end

    let(:ascii8bit_io) do
      io = Tempfile.open
      io.set_encoding(Encoding::ASCII_8BIT, Encoding::ASCII_8BIT)
      io.puts 'ほげほげ'
      io.seek 0
      io
    end

    it 'returns with correct encoding' do
      expect(utf8_io.read.encoding).to eq Encoding::UTF_8
      expect(ascii8bit_io.read.encoding).to eq Encoding::ASCII_8BIT
      ascii8bit_io.seek 0
      expect(ascii8bit_io.read(1).encoding).to eq Encoding::ASCII_8BIT
    end
  end
end
