require 'spec_helper'
require 'ctf'

describe CTF::Math do
  describe '#mod_pow' do
    it 'returns correct value' do
      expect(CTF::Math.mod_pow(2, 5, 10)).to eq 2
    end

    it 'works very big exponent' do
      expect(CTF::Math.mod_pow(3, 2 ** 2048, 81)).to eq 0
      expect(CTF::Math.mod_pow(4, 2 ** 2048, 1000000009)).to eq 409738618
    end
  end

  describe '#sqrt?' do
    it 'return correct value' do
      expect(CTF::Math.sqrt?(100)).to be_truthy
      expect(CTF::Math.sqrt?(101)).to be_falsey
      expect(CTF::Math.sqrt?(0)).to be_truthy
      expect(CTF::Math.sqrt?(1)).to be_truthy
      expect(CTF::Math.sqrt?(-1)).to be_falsey
    end
  end

  describe '#sqrtint' do
    it 'returns correct value' do
      expect(CTF::Math.sqrtint(100)).to eq 10
      expect(CTF::Math.sqrtint(101)).to eq 10
      expect(CTF::Math.sqrtint(120)).to eq 10
      expect(CTF::Math.sqrtint(99)).to eq 9
      expect(CTF::Math.sqrtint(81)).to eq 9
    end
  end
end
